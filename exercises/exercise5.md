## Exercise 5 solutions

```bash
docker container run -d --name psql -v psql:/var/lig/postresql/data postgres:9.6.1
docker container logs -f psql
docker container stop psql
docker container run -d --name psql2 -v psql:/var/lig/postresql/data postgres:9.6.2
docker container logs -f psql2

docker run -p 8083:4000 -v $(pwd):/site bretfisher/jekyll-serve
```