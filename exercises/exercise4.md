## Exercise 4 solutions


cd dockerfile-assignment-1

vim Dockerfile


docker build -t testnode .

docker container run --rm -p 80:3000 testnode

docker images

docker tag --help

docker tag testnode avincze73/testing-node

docker push --help

docker push avincze73/testing-node

docker image ls

docker image rm avincze73/testing-node

docker container run --rm -p 80:3000 avincze73/testing-node