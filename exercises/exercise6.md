## Exercise 6 solutions


```yml
version: '3'
  services:
    drupal:
      image: drupal
      ports:
      - "8080:80"
      volumes:
        - drupal-modules:/var/www/html/modules 
        - drupal-profiles:/var/www/html/profiles 
        - drupal-sites:/var/www/html/sites 
        - drupal-themes:/var/www/html/themes 
    postgres:
      image: postrgres
      environment:
      - POSTGRES_PASSWORD=titkos  
  volumes:
      drupal-modules:
      drupal-profiles:
      drupal-sites:
      drupal-themes:
```


```bash
docker-compose.yml

docker pull drupal

docker image inspect drupal

docker-compose up

https://hub.docker.com

docker-compose down --help

docker-compose down -v
```