#Deploying to kubernetes
```bash
mvn clean package
docker-compose -f nn-currency-exchange/docker-compose.yaml push
docker tag avincze73/nn-currency-conversion:0.0.1-SNAPSHOT avincze73/nn-currency-conversion:first
docker push avincze73/nn-currency-conversion:first


kubectl apply -f nn-currency-conversion.yaml
kubectl logs nn-currency-exchange-86555b6894-g4dqg -f

```
