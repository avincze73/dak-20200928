package hu.sprint.nn.currency.conversion;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "currency-exchange", url = "${CURRENCY_EXCHANGE_URI:http://localhost}:${CURRENCY_EXCHANGE_PORT:8084}/nn-currency-exchange")
public interface CurrencyExchangeServiceProxy {
    @GetMapping("/from/{from}/to/{to}")
    CurrencyConversion retrieveExchangeValue(@PathVariable("from") String from,
                                                        @PathVariable("to") String to);
}