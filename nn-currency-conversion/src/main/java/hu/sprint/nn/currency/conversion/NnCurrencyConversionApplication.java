package hu.sprint.nn.currency.conversion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"hu.sprint.nn.currency.conversion"})
@EnableFeignClients
public class NnCurrencyConversionApplication {

	public static void main(String[] args) {
		SpringApplication.run(NnCurrencyConversionApplication.class, args);
	}

}
