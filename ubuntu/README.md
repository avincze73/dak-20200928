# Setup ubuntu 20.04

## Configuring ubuntu
```bash
sudo apt install -y curl git vim
git config --global credential.helper store
sudo ufw enable
sudo ufw allow ssh
ufw allow 8001
sudo ufw reload
sudo systemctl status ufw.service
sudo ufw status
sudo apt install -y zsh
zsh --version
sudo usermod -s $(which zsh) USERID
sudo chsh -s $(which zsh)
echo $SHELL
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```


## Installing maven
```bash
sudo apt install -y maven
mvn -version
```


## Installing docker
```bash
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose
sudo usermod -aG docker USER
sudo systemctl start docker
sudo systemctl enable docker
#Testing docker
docker run hello-world
```

## Installing gcloud and kubectl
```bash
#snap install google-cloud-sdk --classic
#snap install kubectl --classic
#gcloud init
#gcloud auth list

#echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
#curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
#sudo apt-get update && sudo apt-get install -y google-cloud-sdk


sudo apt-get remove google-cloud-sdk && curl -sSL https://sdk.cloud.google.com | bash -
gcloud init
gcloud config list
gcloud info
gcloud components list
gcloud components update kubectl
source .zshrc
#get the below code from GCP connect to cluster command
gcloud container clusters get-credentials nn-cluster
kubectl config view
kubectl version
```

## Installing mysql in docker
```bash
docker run -d -e MYSQL_ROOT_PASSWORD=Titkos123 -e MYSQL_DATABASE=nn -e MYSQL_USER=nnuser -e MYSQL_PASSWORD=titkos123 -p 3306:3306 mysql:5.7
sudo apt install mysql-client
docker exec -it CONTAINER_ID bash
```

## Installing jenkins
```bash
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt update
apt install jenkins
systemctl start jenkins
systemctl status jenkins
ufw allow 8080
ufw reload
ufw status
```
