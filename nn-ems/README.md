# Commands
```bash
docker tag avincze73/nn-ems:0.0.1-SNAPSHOT avincze73/nn-ems:first
docker push avincze73/nn-ems:first
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker system prune -a
docker run --name mysql  -e MYSQL_ROOT_PASSWORD=Titkos123 -e MYSQL_DATABASE=nn -e MYSQL_USER=nnuser -e MYSQL_PASSWORD=titkos123 -p 63306:3306 mysql:5.7
docker run  -p 58080:8080 --link=mysql --env EMS_HOSTNAME=mysql avincze73/nn-ems:first
```

## Using custom network
```bash
docker network ls
docker network create nn-network
docker inspect nn-network
docker run --name mysql  -e MYSQL_ROOT_PASSWORD=Titkos123 -e MYSQL_DATABASE=nn -e MYSQL_USER=nnuser -e MYSQL_PASSWORD=titkos123 -p 63306:3306 --network=nn-network mysql:5.7
docker run  --name nn-ems -p 58080:8080  --env EMS_HOSTNAME=mysql --network=nn-network  avincze73/nn-ems:first
```

## Deploying to kubernetes
```bash
kompose convert -f docker-compose-ems.yaml
kubectl apply -f mysql-data-persistentvolumeclaim.yaml
kubectl apply -f db-deployment.yaml
kubectl apply -f db-service.yaml
kubectl apply -f nn-ems-deployment.yaml
kubectl apply -f nn-ems-service.yaml
kubectl logs nn-ems-f7d67564c-qm9nm
```


 ## Using configmap
 ```bash
kubectl create configmap nn-ems-config --from-literal=EMS_DB=nn
kubectl get configmap nn-ems-config
kubectl describe configmap/nn-ems-config
kubectl edit configmap/nn-ems-config
kubectl scale deployment nn-ems --replicas=0
kubectl scale deployment nn-ems --replicas=1
kubectl apply -f nn-ems-deployment-2.yaml
kubectl logs nn-ems-5ffc4cf754-5fpxf -f
 ```

 ## Using secrets
 ```bash
kubectl create secret generic nn-ems-secrets --from-literal=EMS_PASSWORD=titkos123
kubectl get secret/nn-ems-secrets
kubectl describe secret/nn-ems-secrets
kubectl apply -f nn-ems-deployment-2.yaml
 ```