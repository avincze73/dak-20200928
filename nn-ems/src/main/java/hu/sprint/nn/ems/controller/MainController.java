package hu.sprint.nn.ems.controller;

import hu.sprint.nn.ems.config.NnProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Slf4j
public class MainController {

    @Autowired
    private NnProperties nnProperties;


    @GetMapping({"/main"})
    public String mainGet(Model model) {
        model.addAttribute("pageTitle", "Főoldal");
        model.addAttribute("nnVersion", nnProperties.getVersion());
        return "main";
    }


    @PostMapping({"/main"})
    public String mainPost(Model model) {
        model.addAttribute("pageTitle", "Főoldal");
        model.addAttribute("nnVersion", nnProperties.getVersion());
        return "main";
    }


    @GetMapping({"/login", "/"})
    public String login(Model model) {
        model.addAttribute("pageTitle", "Főoldal");
        return "login";
    }



}
