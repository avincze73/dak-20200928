package hu.sprint.nn.ems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@SpringBootApplication
@ComponentScan(basePackages = {"hu.sprint.nn.ems.*"})
@EntityScan(basePackages = {"hu.sprint.nn.ems"})
@EnableJpaRepositories(basePackages = {"hu.sprint.nn.ems.repository"})
@EnableRedisHttpSession
public class NnEmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(NnEmsApplication.class, args);
	}

}
