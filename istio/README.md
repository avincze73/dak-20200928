# Setting up istio

```bash
gcloud container clusters get-credentials nn-cluster-istio --zone us-central1-c --project my-second-project-290703
kubectl create namespace istio-system
curl -L https://git.io/getLatestIstio | ISTIO_VERSION=1.2.2 sh -
cd istio-1.2.2
for i in install/kubernetes/helm/istio-init/files/crd*yaml; do kubectl apply -f $i; done
helm template install/kubernetes/helm/istio --set global.mtls.enabled=false --set tracing.enabled=true --set kiali.enabled=true --set grafana.enabled=true --namespace istio-system > istio.yaml
kubectl apply -f istio.yaml

kubectl get pods --namespace istio-system
kubectl get svc --namespace istio-system

kubectl label namespace default istio-injection=enabled
kubectl apply -f nn-greeting.yaml
kubectl logs nn-greeting-55ddc6c9cd-w5tb6 -f #results error
kubectl logs nn-greeting-55ddc6c9cd-w5tb6 nn-greeting -f

#Show istio-ingressgateway
kubectl get svc --namespace istio-system

kubectl apply -f http-gateway.yaml

kubectl apply -f virtualservice-external.yaml

kubectl get svc --namespace istio-system
curl EXTERNAL_IP/greetings


```
# Deployment strategy
```bash
#If deployment strategy is recreate before the new version is deployed the old one should stop and after the new one
#will be started. There is a short down time.

kubectl delete all -l app=nn-greeting
kubectl apply -f merged.yaml

watch -n 0.1 curl 34.121.96.105/greetings-from-instance

kubectl apply -f mirroring.yaml
watch -n 0.1 curl 34.121.96.105/greetings-from-instance


#Canary deployment
kubectl apply -f canary.yaml
watch -n 0.1 curl 34.121.96.105/greetings-from-instance
```


# DUsing 
```bash
#If deployment strategy is recreate before the new version is deployed the old one should stop and after the new one
#will be started. There is a short down time.

kubectl delete all -l app=nn-greeting
kubectl apply -f merged.yaml

watch -n 0.1 curl 34.121.96.105/greetings-from-instance

kubectl apply -f mirroring.yaml
watch -n 0.1 curl 34.121.96.105/greetings-from-instance


#Canary deployment
kubectl apply -f canary.yaml
watch -n 0.1 curl 34.121.96.105/greetings-from-instance
```