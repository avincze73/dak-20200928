#Commands

## Building the application
```bash
mvn clean package
mvn spring-boot:run
```



## docker
```bash
docker images
docker run -p 8080:8080 -d  avincze73/nn-greeting:0.0.1-SNAPSHOT
docker containers
docker ps -a
docker stop CONTAINER_ID
docker login
docker tag avincze73/nn-greeting:0.0.1-SNAPSHOT avincze73/nn-greeting:first
docker push avincze73/nn-greeting:first

docker-compose  -f docker-compose-greeting.yaml build
docker-compose  -f docker-compose-greeting.yaml push
docker-compose  -f docker-compose-greeting.yaml up
```

## kubernetes
```bash
kubectl create deployment nn-greeting --image=avincze73/nn-greeting:first
kubectl get deployments
kubectl get pods
kubectl expose deployment/nn-greeting --type="LoadBalancer" --port 8080
kubectl describe services/nn-greeting
kubectl scale --replicas=3 deployment/nn-greeting


# kubectl run nn-greeting --image=avincze73/nn-greeting:v2 --restart=Never
kubectl autoscale deployment nn-greeting --max=10 --min=3 --cpu-percent=30
kubectl top pods


kubectl api-resources -o wide
kubectl api-versions 
kubectl explain deployments --api-version=apps/v1 --recursive
kubectl explain ingress --api-version=extensions/v1beta1
```

## generating and playing with yaml files
```bash
kubectl get deployments nn-greeting -o wide
kubectl get deployments nn-greeting -o yaml
kubectl get svc nn-greeting -o yaml > service.yaml

kubectl delete all -l app=nn-greeting
kubectl get all
kubectl apply -f merged.yaml
curl 35.193.174.32:8080/greetings-from-instance
kubectl diff -f merged.yaml
kubectl apply -f merged.yaml
kubectl get deployments
brew install watch
curl 35.193.174.32:8080/greetings-from-instance
watch -n 1 curl 35.193.174.32:8080/greetings-from-instance
```

## ReplicationController

Kubernetes replicaset is very similar to deployment but the later is better to use. 
- Scaling vertically (allocate more memory/cpu)
- Scaling horizontally (increase the number of pods)

```bash
kubectl apply -f replication.yaml
kubectl get rc
kubectl get pods
kubectl describe pod nn-greeting-qdmc5
kubectl delete pod nn-greeting-qdmc5
kubectl scale --replicas=5 -f replication.yaml
kubectl scale --replicas=10 -f replication.yaml
kubectl delete rc nn-greeting
```

## Deployment

Replica Set is the next generation of Replication Controller. 
Deployment is easier to use.
- Creating deployment
- Updating deployment
- Rolling updates (zero downtime update)

```bash
kubectl apply -f deployment2.yaml
kubectl get deployments
kubectl get rs
kubectl get pods --show-labels
kubectl rollout status deployment/nn-greeting
kubectl expose deployment/nn-greeting --type="LoadBalancer" --port 8080
kubectl describe service nn-greeting
kubectl set image deployment/nn-greeting nn-greeting=avincze73/nn-greeting:v2
curl 52.142.24.147:8080/greetings-from-instance
kubectl rollout status deployment/nn-greeting
kubectl rollout history deployment/nn-greeting
kubectl rollout undo deployment/nn-greeting
curl 52.142.24.147:8080/greetings-from-instance
kubectl rollout status deployment/nn-greeting
kubectl edit deployment/nn-greeting
kubectl rollout undo  deployment/nn-greeting --to-revision=1
```

## Service

Pods can not be accessed directly but through a Service

```bash
kubectl apply -f nn-greeting/service2.yaml
curl 40.76.154.37:8080/greetings-from-instance
kubectl describe svc nn-greeting
```

## Labels

```bash
kubectl get nodes --show-labels
kubectl get pods
kubectl apply -f deployment-node.yaml
kubectl label nodes aks-agentpool-62943440-vmss000000 hardware=high-spec
kubectl get pods
```

## Health check

- Liveness probe indicates that the container is in running phase. If it is failed the container will be restarted.
- Readiness indicates that the container is ready to serve the request. If the check fails the pods IP address will be removed from the Service so it will not service any request.

```bash
kubectl delete deployment nn-greeting
kubectl apply -f deployment-healthcheck.yaml
kubectl describe pod podid
```


## Service discovery

```bash
kubectl exec -it POD -- mysql -u root -p
kubectl run -it --tty busybox --image=busybox --restart=Never -- sh
nslookup service-name
telnet service-name port

```

## Autoscaling

Comes from dac
```bash
kubectl run -i -t load-generator --image=busybox sh
while true;do wget -q -O- http://nn-greeting:port;done

```

## one service with two deployments
```bash

kubectl delete all -l app=nn-greeting
kubectl get all
kubectl apply -f merged-2.yaml
kubectl get all -o wide
curl 35.193.174.32:8080/greetings-from-instance
kubectl diff -f merged.yaml
kubectl apply -f merged.yaml
kubectl get deployments
brew install watch
curl 35.193.174.32:8080/greetings-from-instance
watch -n 1 curl 35.193.174.32:8080/greetings-from-instance
```

## Setup this project with jenkins (CI).
```bash

```
