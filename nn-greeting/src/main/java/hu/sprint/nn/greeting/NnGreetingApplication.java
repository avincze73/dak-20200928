package hu.sprint.nn.greeting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NnGreetingApplication {

	public static void main(String[] args) {
		SpringApplication.run(NnGreetingApplication.class, args);
	}

}
