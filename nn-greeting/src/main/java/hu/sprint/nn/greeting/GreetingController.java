package hu.sprint.nn.greeting;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class GreetingController {

    @Autowired
    private InstanceInformationService service;

    @GetMapping(path = "/")
    public String imUpAndRunning() {
        return "{healthy:true}";
    }


    @GetMapping(path = "/greetings-from-instance")
    public String greetingsFromInstance() {
        log.info("greetings-from-instance is called");
        return "Greetings from v1v1v1 " + service.retrieveInstanceInfo();
    }

    @GetMapping(path = "/greetings")
    public Greeting greeting() {
        log.info("greetings is called");
        return new Greeting("Greetings from v1v1v1");
    }

    @GetMapping(path = "/greetings/{name}")
    public Greeting greetingPathVariable(@PathVariable String name) {
        log.info("greetings/name is called");
        return new Greeting(String.format("Greetings from v1v1v1, %s", name));
    }
}
