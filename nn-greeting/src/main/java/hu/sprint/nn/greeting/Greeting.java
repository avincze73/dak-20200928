package hu.sprint.nn.greeting;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Greeting {

    private String message;
}
