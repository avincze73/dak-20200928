# Docker and Kubernetes training

## Ubuntu
[Installing Ubuntu 20.04](ubuntu/README.md)

## Commands
[1. Container](commands/container.md)\
[2. Image](commands/image.md)\
[3. Persistent volume](commands/persistent.md)\
[4. Docker compose](commands/compose.md)

## Exercises
[Exercises](exercises/exercises.md)

