# Making It Easier with Docker Compose: The Multi-Container Tool


## Trying Out Basic Compose Commands
```bash
vim docker-compose.yml

docker-compose up

docker-compose up -d

docker-compose logs

docker-compose --help

docker-compose ps

docker-compose top

docker-compose down
```


## Adding Image Building to Compose Files

```bash
docker-compose.yml

docker-compose up

docker-compose up --build

docker-compose down

docker image ls

docker-compose down --help

docker image rm nginx-custom

docker image ls

docker-compose up -d

docker image ls

docker-compose down --help

docker-compose down --rmi local
```

## Assignment Answers: Compose for Run-Time Image Building and Multi-Container Dev
```bash

docker-compose up

docker-compose down

docker-compose up
```