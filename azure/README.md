# Azure
## Setup Azure
```bash
az login 
az aks get-credentials --resource-group sprint --name sprint-cluster
kubectl version
kubectl get nodes
kubectl create deployment nn-greeting --image=avincze73/nn-greeting:second
kubectl get deployment
kubectl expose deployment nn-greeting --type=LoadBalancer --port=8001
kubectl get svc
```

## Setup azure cli
```bash
curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
az login #follow the instructions
az aks get-credentials --resource-group sprint --name sprint-cluster
kubectl get pods
```
