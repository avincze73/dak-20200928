pipeline {
    agent any

    environment {
        DOCKER_USERNAME_2 = credentials("DOCKER_ID_2")
        DOCKER_PASSWORD_2 = credentials("DOCKER_PASSWORD_2")
    }

    stages {

         stage('string (secret text)') {
      steps {
        script {
          withCredentials([
            string(
              credentialsId: 'DOCKER_ID_2',
              variable: 'un')
          ]) {
            print 'un=' + un
            print 'un.collect { it }=' + un.collect { it }
          }
        }
      }
    }





        stage('Build') {
            steps {
                // Get some code from a GitHub repository
                git branch: 'master', credentialsId: 'gitlab-username-password',
                    url: 'https://gitlab.com/avincze73/dak-nn.git'

                // Run Maven on a Unix agent.
                sh "mvn -Dmaven.test.failure.ignore=true -DskipTests clean package"

                // To run Maven on a Windows agent, use
                // bat "mvn -Dmaven.test.failure.ignore=true clean package"
            }

            post {
                // If Maven was able to run the tests, even if some of the test
                // failed, record the test results and archive the jar file.
                success {
                    sh "pwd"
                    //sh "nohup java -jar ./nn-greeting/target/nn-greeting-0.0.1-SNAPSHOT.jar &"

                }
            }
        }

        stage(‘Tag’) {
            steps{
                sh "echo $DOCKER_USERNAME_2"
                sh 'docker tag avincze73/nn-greeting:0.0.1-SNAPSHOT avincze73/nn-greeting:first'
            }
        }

        stage(‘Push’) {
            steps{
                sh 'echo "push to docker hub"'
                sh 'docker login -u $DOCKER_USERNAME_2  -p $DOCKER_PASSWORD_2'
                sh 'docker push avincze73/nn-greeting:first'
            }
        }

        stage(‘Deploy’) {
            steps{
                sh 'echo "logging in" '
                sh 'kubectl apply -f nn-greeting/k8s.yaml'
                sh 'kubectl get pods'
            }
        }
    }
}
