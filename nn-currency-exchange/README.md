#Deploying to kubernetes
```bash
mvn clean package
mvn dockerfile:build
mvn dockerfile:push
docker-compose -f nn-currency-exchange/docker-compose.yaml push
docker tag avincze73/nn-currency-exchange:0.0.1-SNAPSHOT avincze73/nn-currency-exchange:first
docker push avincze73/nn-currency-exchange:first


kubectl apply -f nn-currency-exchange.yaml
kubectl logs nn-currency-exchange-86555b6894-g4dqg -f
```

##Using busybox to get into pods
```bash
kubectl run -i --tty busybox --image=busybox --restart=Never -- sh
uname -a
echo $PATH
wget -qO- 10.0.0.177:8084/nn-currency-exchange
```
