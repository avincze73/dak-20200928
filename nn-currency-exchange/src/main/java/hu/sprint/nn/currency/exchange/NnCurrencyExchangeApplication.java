package hu.sprint.nn.currency.exchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"hu.sprint.nn.currency.exchange"})
@EntityScan(basePackages = {"hu.sprint.nn.currency.exchange"})
@EnableJpaRepositories(basePackages = {"hu.sprint.nn.currency.exchange"})
public class NnCurrencyExchangeApplication {

	public static void main(String[] args) {
		SpringApplication.run(NnCurrencyExchangeApplication.class, args);
	}

}
